import SectionHero from "components/SectionHero/SectionHero";
import SectionSliderNewCategories from "components/SectionSliderNewCategories/SectionSliderNewCategories";
import React from "react";
import SectionSubscribe2 from "components/SectionSubscribe2/SectionSubscribe2";
import SectionOurFeatures from "components/SectionOurFeatures/SectionOurFeatures";
import SectionGridFeaturePlaces from "./SectionGridFeaturePlaces";
import SectionHowItWork from "components/SectionHowItWork/SectionHowItWork";
import BackgroundSection from "components/BackgroundSection/BackgroundSection";
import BgGlassmorphism from "components/BgGlassmorphism/BgGlassmorphism";
import { TaxonomyType } from "data/types";
import SectionGridAuthorBox from "components/SectionGridAuthorBox/SectionGridAuthorBox";
import SectionGridCategoryBox from "components/SectionGridCategoryBox/SectionGridCategoryBox";
import SectionBecomeAnAuthor from "components/SectionBecomeAnAuthor/SectionBecomeAnAuthor";
import SectionVideos from "./SectionVideos";
import SectionClientSay from "components/SectionClientSay/SectionClientSay";
import { Helmet } from "react-helmet";

const DEMO_CATS: TaxonomyType[] = [
  {
    id: "1",
    href: "/listing-stay",
    name: "Riyadh",
    taxonomy: "category",
    count: 210315,
    thumbnail:
      "https://i.ibb.co/Ld95PGd/riyadh-break-free-landing-page-winter-campaign-1920x1080-3.jpg",
  },
  {
    id: "2",
    href: "/listing-stay",
    name: "Jeddah",
    taxonomy: "category",
    count: 120315,
    thumbnail:
      "https://i.ibb.co/D7s0VYq/Jeddah-banner-desktop.png",
  },
  {
    id: "2",
    href: "/listing-stay",
    name: "Tabuk",
    taxonomy: "category",
    count: 23007,
    thumbnail:
      "https://i.ibb.co/jRqshrg/tabuk-break-free-landing-page-winter-campaign-1920x1080-7.jpg",
  },
  {
    id: "2",
    href: "/listing-stay",
    name: "Abha",
    taxonomy: "category",
    count: 31540,
    thumbnail:
      "https://i.ibb.co/XsNgCjh/abha-refresh-winter-campaign-hero-large.jpg",
  },
  {
    id: "2",
    href: "/listing-stay",
    name: "ALOla",
    taxonomy: "category",
    count: 80664,
    thumbnail:
      "https://i.ibb.co/LYnwjVZ/alula-break-free-landing-page-winter-campaign-1920x1080-4.jpg",
  },
  {
    id: "2",
    href: "/listing-stay",
    name: "KingAbullahCity",
    taxonomy: "category",
    count: 8040,
    thumbnail:
      "https://i.ibb.co/dmZWdJw/kaec-break-free-landing-page-winter-campaign-1920x1080-9.jpg",
  },
];

const DEMO_CATS_2: TaxonomyType[] = [
  {
    id: "1",
    href: "/listing-stay",
    name: "RiyadhSeason",
    taxonomy: "category",
    count: 11320,
    thumbnail:
      "https://sba.sa/FileRepository/Channel3/Story/Image/ab76c61f-931d-44c4-b582-3fda70dd4b2f.jpg",
  },
  {
    id: "222",
    href: "/listing-stay",
    name: "JeddahSeason",
    taxonomy: "category",
    count: 8700,
    thumbnail:
      "https://pbs.twimg.com/profile_images/1508426068503449600/O6CG_tja_400x400.jpg",
  },
  {
    id: "3",
    href: "/listing-stay",
    name: "TaiffSeason",
    taxonomy: "category",
    count: 450,
    thumbnail:
      "https://pbs.twimg.com/ext_tw_video_thumb/1169263589548580864/pu/img/k_t96QE6uNBvopA5.jpg",
  },
  {
    id: "4",
    href: "/listing-stay",
    name: "SodahSeason",
    taxonomy: "category",
    count: 1190,
    thumbnail:
      "https://upload.wikimedia.org/wikipedia/ar/f/fa/%D9%85%D9%88%D8%B3%D9%85_%D8%A7%D9%84%D8%B3%D9%88%D8%AF%D8%A9.png",
  },
  {
    id: "5",
    href: "/listing-stay",
    name: "EasternSeason",
    taxonomy: "category",
    count: 3788,
    thumbnail:
      "https://i0.wp.com/www.almuheet.net/wp-content/uploads/poster-1bdbbaf53f2a46c5db012871a91ed9fe.jpg",
  },
];

function PageHome() {
  return (
    <div className="nc-PageHome relative overflow-hidden">
      <Helmet>
        <title>سائح | Visitor</title>
      </Helmet>
      {/* GLASSMOPHIN */}
      <BgGlassmorphism />

      <div className="container relative space-y-24 mb-24 lg:space-y-32 lg:mb-32">
        {/* SECTION HERO */}
        <SectionHero className="pt-10 lg:pt-24 pb-16" />

        {/* SECTION 1 */}
        <SectionSliderNewCategories categories={DEMO_CATS} />

        {/* SECTION2 */}
        {/* <SectionOurFeatures /> */}

        {/* SECTION */}
        <div className="relative py-16">
          <BackgroundSection />
          <SectionGridFeaturePlaces />
        </div>

        {/* SECTION */}
        {/* <SectionHowItWork /> */}

        {/* SECTION 1 */}
        <div className="relative py-16">
          <BackgroundSection className="bg-orange-50 dark:bg-black dark:bg-opacity-20 " />
          <SectionSliderNewCategories
            categories={DEMO_CATS_2}
            categoryCardType="card4"
            itemPerRow={4}
            heading="SaudiSeasonsH"
            subHeading="SaudiSeasonsDesc"
            sliderStyle="style2"
          />
        </div>

        {/* SECTION */}
        {/* <SectionSubscribe2 /> */}

        {/* SECTION */}
        {/* <div className="relative py-16">
          <BackgroundSection className="bg-orange-50 dark:bg-black dark:bg-opacity-20 " />
          <SectionGridAuthorBox />
        </div> */}

        {/* SECTION */}
        {/* <SectionGridCategoryBox /> */}

        {/* SECTION */}
        {/* <div className="relative py-16">
          <BackgroundSection />
          <SectionBecomeAnAuthor />
        </div> */}

        {/* SECTION 1 */}
        {/* <SectionSliderNewCategories
          heading="Explore by types of stays"
          subHeading="Explore houses based on 10 types of stays"
          categoryCardType="card5"
          itemPerRow={5}
        /> */}

        {/* SECTION */}
        {/* <SectionVideos /> */}

        {/* SECTION */}
        {/* <div className="relative py-16">
          <BackgroundSection />
          <SectionClientSay />
        </div> */}
      </div>
    </div>
  );
}

export default PageHome;
