import { MegamenuItem, NavItemType } from "shared/Navigation/NavigationItem";
import ncNanoId from "utils/ncNanoId";
import __megamenu from "./jsons/__megamenu.json";


const megaMenuDemo: MegamenuItem[] = [
  {
    id: ncNanoId(),
    image:
      "https://i.pinimg.com/originals/a9/8b/f9/a98bf90acd67cccb8ed25ff6285a9656.jpg",
    title: "Riyadh",
    items: __megamenu.map((i) => ({
      id: ncNanoId(),
      href: "#",
      name: i.package1,
    })),
  },
  {
    id: ncNanoId(),
    image:
      "https://upload.wikimedia.org/wikipedia/commons/1/15/Jeddah_Corniche_36.jpg",
    title: "Jeddah",
    items: __megamenu.map((i) => ({
      id: ncNanoId(),
      href: "#",
      name: i.package1,
    })),
  },
  {
    id: ncNanoId(),
    image:
      "https://www.ewaahotels.com/uploads/image-slider/03d7703488a58cec3059bc6df945fad41582891535.jpg",
    title: "Abha",
    items: __megamenu.map((i) => ({
      id: ncNanoId(),
      href: "#",
      name: i.package1,
    })),
  },
  {
    id: ncNanoId(),
    image:
      "https://www.arabnews.com/sites/default/files/2021/08/19/2769621-5591712.jpg",
    title: "Tabuk",
    items: __megamenu.map((i) => ({
      id: ncNanoId(),
      href: "#",
      name: i.package1,
    })),
  },
  {
    id: ncNanoId(),
    image:
      "https://www.constructionweekonline.com/cloud/2021/07/07/Maraya-Saudi-Arabia-Al-Ula-credit-MEA-3.jpg",
    title: "ALOla",
    items: __megamenu.map((i) => ({
      id: ncNanoId(),
      href: "#",
      name: i.package1,
    })),
  },
];

const demoChildMenus: NavItemType[] = [
  {
    id: ncNanoId(),
    href: "/",
    name: "Online Booking",
  },
  {
    id: ncNanoId(),
    href: "/home-2",
    name: "Real Estate",
    isNew: true,
  },
];

const otherPageChildMenus: NavItemType[] = [
  {
    id: ncNanoId(),
    href: "/blog",
    name: "Blog Page",
  },
  {
    id: ncNanoId(),
    href: "/blog-single",
    name: "Blog Single",
  },
  {
    id: ncNanoId(),
    href: "/about",
    name: "About",
  },
  {
    id: ncNanoId(),
    href: "/contact",
    name: "Contact us",
  },
  {
    id: ncNanoId(),
    href: "/login",
    name: "Login",
  },
  {
    id: ncNanoId(),
    href: "/signup",
    name: "Signup",
  },
  {
    id: ncNanoId(),
    href: "/subscription",
    name: "Subscription",
  },
];

const templatesChildrenMenus: NavItemType[] = [
  {
    id: ncNanoId(),
    href: "/add-listing-1",
    name: "Add Listings",
    type: "dropdown",
    children: [
      {
        id: ncNanoId(),
        href: "/add-listing-1",
        name: "Add Listings 1",
      },
      {
        id: ncNanoId(),
        href: "/add-listing-2",
        name: "Add Listings 2",
      },
      {
        id: ncNanoId(),
        href: "/add-listing-3",
        name: "Add Listings 3",
      },
      {
        id: ncNanoId(),
        href: "/add-listing-4",
        name: "Add Listings 4",
      },
      {
        id: ncNanoId(),
        href: "/add-listing-5",
        name: "Add Listings 5",
      },
      {
        id: ncNanoId(),
        href: "/add-listing-6",
        name: "Add Listings 6",
      },
      {
        id: ncNanoId(),
        href: "/add-listing-7",
        name: "Add Listings 7",
      },
      {
        id: ncNanoId(),
        href: "/add-listing-8",
        name: "Add Listings 8",
      },
      {
        id: ncNanoId(),
        href: "/add-listing-9",
        name: "Add Listings 9",
      },
      {
        id: ncNanoId(),
        href: "/add-listing-10",
        name: "Add Listings 10",
      },
    ],
  },
  //
  { id: ncNanoId(), href: "/checkout", name: "Checkout" },
  { id: ncNanoId(), href: "/pay-done", name: "Pay done" },
  //
  { id: ncNanoId(), href: "/author", name: "Author Page" },
  { id: ncNanoId(), href: "/account", name: "Account Page" },
];

export const NAVIGATION_DEMO: NavItemType[] = [
  {
    id: ncNanoId(),
    href: "/",
    name: "Home",
    type: "dropdown",
    children: demoChildMenus,
  },
  {
    id: ncNanoId(),
    href: "#",
    name: "Packages",
    type: "megaMenu",
    megaMenu: megaMenuDemo,
  },
  {
    id: ncNanoId(),
    href: "#",
    name: "Services",
    type: "dropdown",
    children: [
      { id: ncNanoId(), href: "/listing-stay", name: "Stay page" },
      { id: ncNanoId(), href: "/listing-stay-map", name: "Stay page (map)" },
      { id: ncNanoId(), href: "/listing-stay-detail", name: "Stay Detail" },
      //
      {
        id: ncNanoId(),
        href: "/listing-experiences",
        name: "Experiences page",
      },
      {
        id: ncNanoId(),
        href: "/listing-experiences-map",
        name: "Experiences page (map)",
      },
      {
        id: ncNanoId(),
        href: "/listing-experiences-detail",
        name: "Experiences Detail",
      },
      //
      { id: ncNanoId(), href: "/listing-car", name: "Cars page" },
      { id: ncNanoId(), href: "/listing-car-map", name: "Cars page (map)" },
      { id: ncNanoId(), href: "/listing-car-detail", name: "Car Detail" },
      //
      {
        id: ncNanoId(),
        href: "/listing-real-estate",
        name: "Real Estate Listings",
      },
      {
        id: ncNanoId(),
        href: "/listing-real-estate-map",
        name: "Real Estate Maps",
      },
    ],
  },
  // {
  //   id: ncNanoId(),
  //   href: "#",
  //   name: "Templates",
  //   type: "dropdown",
  //   children: templatesChildrenMenus,
  // },

  // {
  //   id: ncNanoId(),
  //   href: "#",
  //   name: "Other pages",
  //   type: "dropdown",
  //   children: otherPageChildMenus,
  // },
  {
    id:ncNanoId(),
    href: "#",
    name: "test",
    type: 'checkBox',
  }
];
